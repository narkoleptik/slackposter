on run argv
	if (count of argv) > 0 then
		set slackchannel to item 1 of argv
		set message to item 2 of argv
	else
		set slackchannel to text returned of (display dialog "Slack Channel:" default answer "")
		set message to text returned of (display dialog "Message:" default answer "")
	end if
	
	tell application "Slack"
		activate
		tell application "System Events"
			keystroke "k" using {command down}
			delay 0.5
			keystroke slackchannel
			delay 0.5
			key code 36
			delay 0.5
			keystroke message
			delay 0.5
			key code 36
			delay 0.5
			
		end tell
	end tell
	tell application "iTerm" to activate
end run
